from sanic import Sanic
from sanic import response
import json, logging, asyncio
import socketio
from Jobs import functions
from config import Config
from jinja2 import Environment, FileSystemLoader


app = Sanic(__name__)
app.static('/static', './static')

sio = socketio.AsyncServer(async_mode='sanic')
sio.attach(app)



# define the environment for the Jinja2 templates
env = Environment(loader=FileSystemLoader('pages/'))


@app.route('/')
async def index(request):
    template = env.get_template( 'test_files.html' )
    page     = template.render(workers=Config.QUEUES)
    return response.html( page )


@app.route('/submit_job', methods=['POST'])
async def submit_job(request):     
    try:
        submission   = request.files.get('submission')
        testing_code = request.files.get('testing_code')
        worker       = request.form.get('worker')
        
        submission   = submission.body  if submission   is not None else None
        testing_code =testing_code.body if testing_code is not None else None
        
        result = functions.submit_job(submission, testing_code, worker )
        logging.debug('Result = {}'.format(result))
    except Exception as e:
        logging.exception(e)
        result = {'job_ID':None, 'status':'FAILED', 'results':'Failed to submit job because {}'.format(e)}
    return response.json(result)

@app.route('/get_job_status', methods=['GET'])
async def get_job_status(request):
    results = functions.get_job_status( request.args['job_ID'][0] )
    return response.json(results)

  

@sio.event
async def socket_job_status(sid, job_ID):
    last_status    = {}
    current_status = functions.get_job_status(job_ID)
    
    while current_status['status'] not in ['FINISHED', 'EXPIRED', 'FAILED']:
        if current_status != last_status:
            await sio.emit( 'job_status', current_status , room=sid)
            last_status = current_status
        await asyncio.sleep(0.5)
        current_status = functions.get_job_status(job_ID)
    
    await sio.emit( 'job_status', current_status , room=sid)
#    jobs = []
#    worker       = data.get('testing_code')['worker']
#    testing_code = data.get('testing_code')['file']
#    
#    for submission in data.get('submissions', []):
#        submission_file = submission['file']
#        submission_ID   = submission.get('submission_ID')
#        
#        try:
#            job = functions.submit_job(submission_file, testing_code, worker )
#            job['submission_ID'] = submission_ID
#        except Exception as e:
#            logging.exception(e)
#            job = {'job_ID':None, 'status':'FAILED', 'results':'Failed to submit job because {}'.format(e)}
#        

#        if job['job_ID'] is not None:
#            jobs.append(job)
#        await sio.emit( 'job_submitted', job , room=sid)
#    
#    while len(jobs) != 0:
#        i = 0
#        while i < len(jobs):
#            job = jobs[i]
#            job_ID = job['job_ID']
#            job_status = functions.get_job_status(job_ID)
#            job_status['submission_ID'] = job['submission_ID']
#            if job_status['status'] not in ['PENDING', 'RUNNING']:
#                jobs.pop(i)
#            else:
#                i += 1
#            
#            await sio.emit( 'job_status', job_status , room=sid)
#            await asyncio.sleep(0.1)
#        await asyncio.sleep(1)
    
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)
