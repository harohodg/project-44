/**
    Functions used to display the progress bar
**/

/*
    Function to set prog bar message text
*/
function set_progbar_text(text){
    $('.progbar-msg').html(text);    
}

/*
    Function to show progress bar message text
*/
function show_progbar_text(){
    $('.progbar-msg').removeClass('hidden');
}


/*
    Function to initialize progressbar.
*/
function init_progbar(bar_max){
    $('.progressbar').progressbar({
          max:bar_max,
          complete: function(){$('#download-json').removeAttr('hidden'); $('#download-csv').removeAttr('hidden'); },
          //change : function(){ console.log( $( ".progressbar" ).progressbar( "value") ) },
          value:0
        });
}

/*
    Function to increment progressbar
*/
function increment_progbar(){
    $( ".progressbar" ).progressbar( "value",  $( ".progressbar" ).progressbar( "value" )+1 );
}


/*
    Function to set bar value
*/
function set_progbar_value(bar_value){
    $('.progressbar').progressbar({
          value:bar_value
        });
}


/*
    Function to show loader
*/
function show_progbar(){
    $('.progressbar').removeAttr('hidden');
}

/*
    Function to hide loader
*/
function hide_progbar(){
    $('.progressbar').addAttr('hidden');
}
