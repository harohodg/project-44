// socket
var socket = io.connect();

socket.on('connect', function() {
      //alert('Connected')
      $('.job').find('.status:contains(PENDING), .status:contains(RUNNING)').parent().each( function(index){ socket.emit('socket_job_status', $(this).attr('id') ) })
  });
  
  socket.on('job_status', function(msg, cb) {
    updateJobState(msg.job_ID, msg.status, msg.results);
    if( $.inArray( msg.status, [ "FINISHED", "FAILED"] ) != -1 ){
        //alert(msg.status);
        increment_progbar();
    }
  });


function updateJobState(job_ID, status, results){
    $('#'+job_ID).find('.status').attr('status', status)
    $('#'+job_ID).find('.status').text(status)
    
    errors  = status == 'FAILED' ?  JSON.stringify(results) : '';
    results = status != 'FAILED' ?  JSON.stringify(results): '';
    
    $('#'+job_ID).find('.errors').text( errors  )
    $('#'+job_ID).find('.results').text( results )
}



function submitJob(submission_ID, submission, testing_code, worker){
    formdata = new FormData();
    formdata.append("submission", submission);
    formdata.append("testing_code", testing_code);
    formdata.append("worker", worker);
    
    $.ajax({
        type : 'POST',
        url  : 'submit_job',
        cache: false,
        processData: false,
        contentType: false,
        data: formdata,
        dataType:'json',
        success:function(response){ $('.submission-id-' + submission_ID).attr('id', response.job_ID); updateJobState(response.job_ID, response.status, response.results); socket.emit('socket_job_status', response.job_ID) },
        error:function(xhr,status,error){ $('.submission-id-' + submission_ID).attr('id', submission_ID); updateJobState(submission_ID, status, error) },
        });
}

function submitJobs( submissions_files, testing_code_file, worker ){
    init_progbar( submissions_files.length );
    show_progbar();
    $.each(submissions_files, function( index, submission ) {
        if (submission.size == 0){
            $('.submission-id-' + index).attr('id', index)
            updateJobState(index, 'FAILED', 'File size is zero bytes')
            increment_progbar()
        } else {
            submitJob(index, submission, testing_code_file, worker)
        }     
    })    

}

function addRowToTable(submission_ID, filename){
    new_row = $('.table-template-row').clone(true, true);
    new_row.removeClass('table-template-row');
    new_row.removeAttr('hidden')

    new_row.find('.file-name').html(filename);

    var classname = 'submission-id-' + submission_ID;
    new_row.addClass(classname)
    new_row.addClass('job')


    $('.table-template-row').before(new_row);
    
}

function populateTable(submissions){
    $( ".job" ).remove();
    $( "#download-json" ).attr("hidden",true);
    $( "#download-csv"  ).attr("hidden",true);
    $.each(submissions, function( index, file ) {
        addRowToTable(index, file.name)       
    });
}

function showTable(){
    $('table').removeAttr('hidden')
}

function uploadFiles(evt) {

    var submissions_files = Array.from( document.getElementById('submission').files );
    var testing_code_file = document.getElementById('testing_code').files[0];
    var worker = $("input[name='worker']:checked").val();


    if (submissions_files && testing_code_file && worker) {
        populateTable( submissions_files );
        showTable();
        
        submitJobs( submissions_files, testing_code_file, worker );   


    } else {
        alert('Failed to submit job(s). Unable to find files to send')
    }
}


