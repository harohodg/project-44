$( document ).ready(function() {
    $("#download-json").click(function(){
        link = this;
        var uri = 'data:application/csv;charset=UTF-8,' + encodeURIComponent( JSON.stringify( tableToJSON() ) );
        $(link).attr("href", uri);   
    });

    $("#download-csv").click(function(){
        link = this;
        csv_table = tableToCSV().sort(function(a, b){
              // ASC  -> a.length - b.length
              // DESC -> b.length - a.length
              return $.map( b, function(v){ return 1 } ).length - $.map( a, function(v){ return 1 } ).length;
            });
        var uri = 'data:application/csv;charset=UTF-8,' + encodeURIComponent( Papa.unparse( csv_table, {'quotes':true, 'delimiter':'|'}) );
        $(link).attr("href", uri);   
    });
});

function tableToJSON(){
    return $('table tr.job').get().map(function(row) {
        job_ID    = $(row).attr('id');
        file_name = $(row).find('td.file-name').text();
        status    = $(row).find('td.status').text();
        errors    = $(row).find('td.errors').text();
        results   = $(row).find('td.results').text();
        
        errors    = errors  != '' ? JSON.parse( errors ) : '';
        try{
            results   = results != '' ? JSON.parse( results ) : '';
        } catch(err) {
            alert(file_name + ' results are not proper JSON.')
            console.log(err.message);
            console.log(file_name);
            console.log(results);
        }
        
      return {'job_ID' : job_ID, 'filename' : file_name , 'status' : status, 'errors' : errors, 'results' : results}
    });
}

function tableToCSV(){
    return $.map(tableToJSON(), function(value){
        var results = value.results;
        delete value.results;
        return $.extend({}, value, results);
    });
}


