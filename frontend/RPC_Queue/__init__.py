"""
    Modified from the code at https://github.com/eandersson/python-rabbitmq-examples/blob/master/Flask-examples/pika_async_rpc_example.py
    and https://www.rabbitmq.com/tutorials/tutorial-six-python.html
"""
import pika, json
import uuid
import threading, logging
from time import sleep
from expiringdict import ExpiringDict

class RPC_Queue(object):
    """Asynchronous Rpc client."""
    internal_lock = threading.Lock()
    results = ExpiringDict(max_len=1000, max_age_seconds=600)
    jobs    = {}
  

    def __init__(self):
        """Set up the basic connection, and start a new thread for processing.
            1) Setup the pika connection, channel and queue.
            2) Start a new daemon thread.
        """
        connection_parameters = pika.ConnectionParameters(host='rabbit', retry_delay=5, connection_attempts=12 )
        self.connection = pika.BlockingConnection( connection_parameters )
        self.channel = self.connection.channel()
        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue
        thread1 = threading.Thread(target=self._process_data_events)
        thread1.setDaemon(True)
        thread1.start()
        
        thread2 = threading.Thread(target=self._process_jobs)
        thread2.setDaemon(True)
        thread2.start()

    def _process_data_events(self):
        """Check for incoming data events.
        We do this on a thread to allow the flask instance to send
        asynchronous requests.
        It is important that we lock the thread each time we check for events.
        """
        try:
            self.channel.basic_consume(on_message_callback=self._on_response, queue=self.callback_queue)
        except TypeError as e:
            print('Failed to start consuming because {}'.format(e))
            print('callback_queue = {}'.format(self.callback_queue))
        while True:
            with self.internal_lock:
                self.connection.process_data_events()
            sleep(0.5)

    def _on_response(self, ch, method, props, body):
        """On response we simply store the result in a local dictionary."""
        self.results[props.correlation_id] = json.loads(body)
        self.channel.basic_ack(delivery_tag=method.delivery_tag)

    def _queue_job(self, target, job, job_ID):
        """Function to add job to queue"""
        with self.internal_lock:
            self.channel.basic_publish(exchange='',
                                       routing_key=target,
                                       properties=pika.BasicProperties(
                                           reply_to=self.callback_queue,
                                           correlation_id=job_ID,
                                       ),
                                       body=json.dumps(job))
    
    def _process_jobs(self):
        """Function to check for pending jobs and submit them"""
        while True:
            while len(self.jobs) != 0:
                job_ID, job_details = self.jobs.popitem()
                self._queue_job(job_details['target'], job_details['job'], job_ID)
                sleep(0.1) 
                  
    
    def send_request(self, target, payload):
        """Send an asynchronous Rpc request.
        The main difference from the rpc example available on rabbitmq.com
        is that we do not wait for the response here. Instead we let the
        function calling this request handle that.
            corr_id = rpc_client.send_request(payload)
            while RPC_Queue.results[corr_id] is None:
                sleep(0.1)
            return RPC_Queue.results[corr_id]
        If this is a web application it is usually best to implement a
        timeout. To make sure that the client wont be stuck trying
        to load the call indefinitely.
        We return the correlation id that the client then use to look for
        responses.
        """
        corr_id = str(uuid.uuid4())
        self.results[corr_id] = {'job_ID':corr_id, 'status':'PENDING', 'results' : None}
        self.jobs[corr_id]    = {'target':target, 'job':payload}
        
        return corr_id
