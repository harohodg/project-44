from RPC_Queue import RPC_Queue
from config import Config

QUEUES = Config.QUEUES

queue = RPC_Queue()


def submit_job(submission, testing_code, worker):
    assert submission is not None and testing_code is not None, 'submission or testing_code is missing'
    assert len(submission)  != 0  and len(testing_code) != 0, 'no file provided for submission or testing_code'
    assert worker is not None and worker in QUEUES, 'worker must be on of [' + ','.join(QUEUES) + ']'
    
    
    data_to_send = {'submission':submission.hex(), 'testing_code':testing_code.hex()}

    corr_id = queue.send_request(worker, data_to_send)
    return queue.results[corr_id]
    

def get_job_status(job_ID):
    return queue.results.get(job_ID) if job_ID in queue.results else {'job_ID':job_ID, 'status':'EXPIRED','results': 'Job ID was not found on server.'}
