# Project 44

Welcome to Project 44[^1] an autograding engine which takes in files and returns test results. The engine is language agnostic [^2], as long as you provide it with files in the correct format it will try to test them.
A key feature of Project 44 is that tests are run in indiviual sandboxes in parallel. This minimizes the damage that rouge [^3] [^4] code can do intentionally or by accident. This is achieved by using a Queuing system combined with Docker containers. 

Conceptually this means separating the code into several [Docker](https://www.docker.com/) containers each dealing with a specific [separate concern](https://en.wikipedia.org/wiki/Separation_of_concerns). Specifically

* the frontend which exposes an API for submitting jobs and querying their status, as well as a simple user interface for submitting jobs via a browser.
* the backend which handles the actual testing and is further subdivided into 
    - a [RabbitMQ](https://www.rabbitmq.com/) [^5] server for distributing the submissions to actual testing units for testing.
	- testing units which do the actual testing in temporary folders which are destroyed after the test is finished and the results returned. The included testing unit support Python3 code with no extra libraries. See below for how to add
	additional custom testing units

For now the engine takes anonymous submissions. In future versions it may require some form of authentication before it accepts files for testing. The engines only job is testing, it is the responsibility of those submitting jobs[^7] to ask for the results and to resubmit the job if the engine crashed or forgot the results. 
Results are currently stored for a max of 600 seconds or 1000 results. This will likely change in future or at least be user definable.

## Testing Units
Testing units take two zip files (submission and testing code) and extract them into /some temp folder/submission_files and /some temp folder/testing_files respectively. They then run /some temp folder/testing_files/run_tests.sh. Afterwards they
return the contents of /some temp folder/testing_files/results.json

Note : run_tests.sh does not have to be a bash script file. As long as it starts with #!/usr/bin/env program_name (eg. #!/usr/bin/env python3) the testing unit can run it.


## API
The engine exposes two http routes to users.

- /submit_job [POST]
    - takes a submission zip file, a testing code zip file and a queue name and returns a job ID which is a hashed combination of the current timestamp. The queue is used to send the code to an appropriate testing unit.
- /get_job_status [GET]
    - takes a job ID and returns its status ( one of PENDING, RUNNING, FAILED, SUCCESSFULL, EXPIRED ) and results. For PENDING, RUNNING, and EXPIRED results = null
    - PENDING  : the job is waiting for a testing unit to run it
	- RUNNING  : the job is now running on a testing unit
	- FAILED   : something went wrong on the testing unit. results = any error messages
	- FINISHED : the job finished successsfully. results = what the testing unit returned.
	- EXPIRED  : the job_ID does not exist in the database
	
It also exposes one socket-io event for getting job status. 	
	
Once a job as finished (successfully or failed) it is marked for destruction after 10 minutes. In future after a user has called /get_job_status and FAILED OR FINISHED was returned jobs may have their destruction deadline lowered.


### Example Usage
#### submit job and run on test unit with Python 3X installed.
- curl : curl -F 'submission=@/home/harohodg/submission.zip' -F 'testing_code=@/home/harohodg/testing_code.zip' -F 'worker=python' localhost:8000/submit_job
- python requests : r = requests.post('http://localhost:8044/submit_job', files=files, data=values) where files = {'submission' : submission, 'testing_code' : testing_code} and values = {'worker' : 'some_queue'}
- result = {"job_ID":"eb90cb3c-d90a-4ccb-9236-2cdf76e088eb","status":"PENDING","results":null} or {"job_ID":null,"status":"FAILED","results":"Failed to submit job because ..."}

#### get job status
- curl : curl localhost:8000/get_job_status?job_ID=eb90cb3c-d90a-4ccb-9236-2cdf76e088eb
- python requests : r = requests.get('http://localhost:8044/get_job_status?job_ID={}'.format(job_ID) )
- result = {"job_ID":"eb90cb3c-d90a-4ccb-9236-2cdf76e088eb","status":"FINISHED","results":...} or {"job_ID":"eb90cb3c-d90a-4ccb-9236-2cdf76e088eb","status":"EXPIRED","results":"Job ID was not found on server."}


## Deployment
It  is assumed that if you want to use Project 44 you have Docker and [Docker Compose](https://docs.docker.com/compose/) installed [^8]. We also recommend running a Docker GUI such as [Portainer](https://www.portainer.io/) to interact with the Project 44 containers.
If you do not want the engine accessable to the outside world you will need to add authentication (using something like an Apache reverse proxy) or a firewall to your system.

1) Start by downloading the desired version from [here](https://bitbucket.org/harohodg/project-44/downloads/?tab=tags) and extract it somewhere on your server

2) Edit the .env file to change any cpu or memory limits if so desired. Then run ./deploy_engine.sh from the project root. To stop the system run ./stop_engine.sh from the same folder. The first time will take awhile as Docker builds the base images.

3) Once up you should be able to go to servername:8000/get_job_status?job_ID=42 in a browser and it should return {"job_ID":"42","status":"EXPIRED","results":"Job ID was not found on server."}

4) Or go to servername:8000 and you should see a page where you can upload multiple files at once.


### Adding Testing Units
To be filled in.


[^1]: Name subject to change. Chosen because Project 43 already exists.

[^2]: It does't even have to be a programming language like C or Java.

[^3]: As [Humpty Dumpty](https://sabian.org/looking_glass6.php) might say. 
[^4]: Normals perhaps would call it rogue code. 

[^5]: Chosen after several hours of reading and tinkering with [various](http://queues.io/) queueing systems. Ultimately chosen because it has decent documentation, is actively updated, and rather importantly **just works** with regards to what we [^6]
need it to do. Does it __kill flies with atomic bombs__? Likely, but given that [good code](https://xkcd.com/844/) is hard write we [^6] prefer to have something that works and worry about performance later.

[^6]: The royal we of course. See [^3].

[^7]: Human or otherwise.

[^8]: It is recommended that you set Docker up to not require root access to run it. See [here](https://docs.docker.com/install/linux/linux-postinstall/) for details. 
