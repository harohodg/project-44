import pika, json, tempfile, os, stat, time
import zipfile, logging
import io
from random import randint



def send_message(channel, queue, correlation_id, message):
    channel.basic_publish(exchange='',
                     routing_key=queue,
                     properties=pika.BasicProperties(correlation_id = correlation_id),
                     body=json.dumps(message))

def run_tests(ch, method, props, body):
    logging.info(" [worker] Received some data")

    job_ID = props.correlation_id
    response = {'job_ID':job_ID, 'status':'RUNNING','results':None}
    send_message(ch, props.reply_to, job_ID, response )


    try:
        testing_data  = json.loads( body.decode('utf-8') )

        testing_code    = bytes.fromhex( testing_data['testing_code'] )
        submission_data = bytes.fromhex( testing_data['submission'] )


        #Dump to disk
        with tempfile.TemporaryDirectory() as temp_directory:
            logging.debug('Created temporary directory {}'.format(temp_directory) )

            submission_directory = '{}/submission_files'.format(temp_directory)
            testing_directory    = '{}/testing_files'.format(temp_directory)

            os.mkdir( submission_directory )
            os.mkdir( testing_directory )

            submission = zipfile.ZipFile(io.BytesIO(submission_data))
            testing    = zipfile.ZipFile(io.BytesIO(testing_code))

            assert 'run_tests.sh' in testing.namelist(), 'template must include run_tests.sh'

            submission.extractall( submission_directory )
            testing.extractall( testing_directory )


            testing_file = '{}/run_tests.sh'.format(testing_directory) 

            os.chdir( testing_directory )
            st = os.stat(testing_file)
            os.chmod(testing_file, st.st_mode | stat.S_IEXEC)
            os.system( testing_file )

            time.sleep(2)
            logging.debug(os.listdir(testing_directory))

            with open( '{}/testing_files/results.json'.format(temp_directory), 'r' ) as results_file:
                response['results'] = json.load(results_file)
                response['status']  = 'FINISHED'
        

    except Exception as e:
        logging.error("Something went wrong : {}".format(e) )
        response['status']  = 'FAILED'
        response['results'] = str(e)
    finally:
        send_message(ch, props.reply_to, job_ID, response )
        ch.basic_ack(delivery_tag=method.delivery_tag)     
