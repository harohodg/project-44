#!/usr/bin/env python
import pika, os
import functions

QUEUE = os.environ.get('QUEUE') or 'submissions'

connection_parameters = pika.ConnectionParameters(host='rabbit', retry_delay=5, connection_attempts=12 )
connection = pika.BlockingConnection( connection_parameters )

channel = connection.channel()
channel.queue_declare(queue=QUEUE, durable=True)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=QUEUE, on_message_callback=functions.run_tests)

print(" [x] Awaiting RPC requests")
channel.start_consuming()
